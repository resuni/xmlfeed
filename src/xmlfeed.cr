require "./atom"
require "./rss"

require "xml"

module XMLFeed

  def parse(xml : String)

    # Parse XML.
    xml_node = XML.parse(xml)
  
    # Get child names from top level of XML.
    top_elements = Array(String).new
    xml_node.children.each do |child|
      top_elements.push(child.name)
    end
  
    # If "feed" is a top level element, it's probably Atom.
    # If "rss" is a top level element, it's probably RSS.
    if top_elements.includes?("feed")
      @object = Atom.new(xml_node)
    elsif top_elements.includes?("rss")
      @object = RSS.new(xml_node)
    else
      raise UnknownFeedType.new("Unable to determine feed type")
    end

  end

  class UnknownFeedType < Exception
  end
  
end


require "json"

# Based on RFC4287
# https://tools.ietf.org/html/rfc4287

module Atom

  # https://tools.ietf.org/html/rfc4287#section-2
  class CommonAttributes

    include JSON::Serializable

    getter uri : String?
    getter lang : String?

    def parse_common_attributes(xml_node : XML::Node)
      @uri = xml_node["uri"]?
      @lang = xml_node["lang"]?
    end

  end

  # https://tools.ietf.org/html/rfc4287#section-3.1
  class Text

    include JSON::Serializable

    getter type : String?
    getter text : String

    def initialize(xml_node : XML::Node)
      @type = xml_node["type"]?
      @text = xml_node.inner_text
    end

  end

  # https://tools.ietf.org/html/rfc4287#section-3.2
  class Person

    include JSON::Serializable

    getter name : Text?
    getter uri : String?
    getter email : String?

    def initialize(xml_node : XML::Node)
      xml_node.children.select(&.element?).each do |child|
        case child.name
          when "name"
            @name = Text.new(child)
          when "uri"
            @uri = child.inner_text
          when "email"
            @email = child.inner_text
        end
      end
    end

  end

  # https://tools.ietf.org/html/rfc4287#section-3.3
  class DateConstruct < CommonAttributes

    getter date : Time

    def initialize(xml_node : XML::Node)
      parse_common_attributes(xml_node)
      @date = Time.parse_rfc3339(xml_node.inner_text)
    end
  end

  # https://tools.ietf.org/html/rfc4287#section-4.2.11
  class Source < CommonAttributes

    getter authors : Array(Person) = Array(Person).new
    getter categories : Array(Category) = Array(Category).new
    getter contributors : Array(Person) = Array(Person).new
    getter generator : Generator?
    getter icon : Icon?
    getter id : ID?
    getter links : Array(Link) = Array(Link).new
    getter logo : Logo?
    getter rights : Text?
    getter subtitle : Text?
    getter title : Text?
    getter updated : DateConstruct?

    def initialize(xml_node : XML::Node)

      # Parse common attributes.
      parse_common_attributes(xml_node)

      # Parse elements.
      xml_node.children.select(&.element?).each do |child|
        parse_common_elements(child)
      end

    end

    # Function parses elements common to Atom::Source and Atom::Feed.
    def parse_common_elements(child : XML::Node)
      case child.name
        when "author"
          @authors.push(Person.new(child))
        when "category"
          @categories.push(Category.new(child))
        when "contributor"
          @contributors.push(Person.new(child))
        when "generator"
          @generator = Generator.new(child)
        when "icon"
          @icon = Icon.new(child)
        when "id"
          @id = ID.new(child)
        when "link"
          @links.push(Link.new(child))
        when "logo"
          @logo = Logo.new(child)
        when "rights"
          @rights = Text.new(child)
        when "subtitle"
          @subtitle = Text.new(child)
        when "title"
          @title = Text.new(child)
        when "updated"
          @updated = DateConstruct.new(child)
      end
    end

  end

  # https://tools.ietf.org/html/rfc4287#section-4.1.1
  class Feed < Source

    getter entries : Array(Entry) = Array(Entry).new

    def initialize(xml_node : XML::Node)

      # Extract feed from parsed XML.
      xml_node = xml_node.children.select(&.name.matches?(/^feed$/))[0]

      # Parse common attributes.
      parse_common_attributes(xml_node)

      # Parse elements.
      xml_node.children.select(&.element?).each do |child|

        # Parse entries.
        if child.name == "entry"
          @entries.push(Entry.new(child))
        end

        # Parse common elements.
        parse_common_elements(child)
  
      end

    end

  end

  # https://tools.ietf.org/html/rfc4287#section-4.1.2
  class Entry < CommonAttributes

    getter authors : Array(Person) = Array(Person).new
    getter categories : Array(Category) = Array(Category).new
    getter content : Content?
    getter contributors : Array(Person) = Array(Person).new
    getter id : ID?
    getter links : Array(Link) = Array(Link).new
    getter published : DateConstruct?
    getter rights : Text?
    getter source : Source?
    getter summary : Text?
    getter title : Text?
    getter updated : DateConstruct?

    def initialize(xml_node : XML::Node)

      # Parse common attributes.
      parse_common_attributes(xml_node)

      xml_node.children.select(&.element?).each do |child|
        case child.name
          when "author"
            @authors.push(Person.new(child))
          when "category"
            @categories.push(Category.new(child))
          when "content"
            @content = Content.new(child)
          when "contributor"
            @contributors.push(Person.new(child))
          when "id"
            @id = ID.new(child)
          when "link"
            @links.push(Link.new(child))
          when "published"
            @published = DateConstruct.new(child)
          when "rights"
            @rights = Text.new(child)
          when "source"
            @source = Source.new(child)
          when "summary"
            @summary = Text.new(child)
          when "title"
            @title = Text.new(child)
          when "updated"
            @updated = DateConstruct.new(child)
        end
      end

    end
  end

  # https://tools.ietf.org/html/rfc4287#section-4.1.3
  class Content < CommonAttributes

    getter type : String? # Important note in section 4.1.3.1
    getter src : String?
    getter text : String

    def initialize(xml_node : XML::Node)
      parse_common_attributes(xml_node)
      @type = xml_node["type"]?
      @src = xml_node["src"]?
      @text = xml_node.inner_text
    end
  end

  # https://tools.ietf.org/html/rfc4287#section-4.2.2
  class Category < CommonAttributes

    getter term : String?
    getter scheme : String?
    getter label : String?

    def initialize(xml_node : XML::Node)
      parse_common_attributes(xml_node)
      @term = xml_node["term"]?
      @scheme = xml_node["scheme"]?
      @label = xml_node["label"]?
    end
  end

  # https://tools.ietf.org/html/rfc4287#section-4.2.4
  class Generator < CommonAttributes

    getter version : String?
    getter text : String

    def initialize(xml_node : XML::Node)
      parse_common_attributes(xml_node)
      @version = xml_node["version"]?
      @text = xml_node.text
    end
  end

  # https://tools.ietf.org/html/rfc4287#section-4.2.5
  class Icon < CommonAttributes

    getter text : String

    def initialize(xml_node : XML::Node)
      parse_common_attributes(xml_node)
      @text = xml_node.inner_text
    end
  end

  # https://tools.ietf.org/html/rfc4287#section-4.2.6
  class ID < CommonAttributes

    getter text : String

    def initialize(xml_node : XML::Node)
      parse_common_attributes(xml_node)
      @text = xml_node.inner_text
    end
  end

  # https://tools.ietf.org/html/rfc4287#section-4.2.7
  class Link < CommonAttributes

    getter href : String?
    getter rel : String = "alternate"
    getter type : String?
    getter hreflang : String?
    getter title : String?
    getter length : String?

    def initialize(xml_node : XML::Node)
      parse_common_attributes(xml_node)
      @href = xml_node["href"]?
      begin
        @rel = xml_node["rel"]
      rescue KeyError
      end
      @type = xml_node["type"]?
      @hreflang = xml_node["hreflang"]?
      @title = xml_node["title"]?
      @length = xml_node["length"]?
    end
  end

  # https://tools.ietf.org/html/rfc4287#section-4.2.8
  class Logo < CommonAttributes

    getter text : String

    def initialize(xml_node : XML::Node)
      parse_common_attributes(xml_node)
      @text = xml_node.inner_text
    end
  end

end

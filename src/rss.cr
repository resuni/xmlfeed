require "json"

# Based on https://www.rssboard.org/rss-specification

module RSS

  # https://www.rssboard.org/rss-specification#requiredChannelElements
  class Channel
    include JSON::Serializable

    # Required elements.
    getter title : String?
    getter link : String?
    getter description : String?

    # Optional elements.
    getter items : Array(Item) = Array(Item).new
    getter language : String?
    getter copyright : String?
    getter managing_editor : String?
    getter web_master : String?
    getter pub_date : Time?
    getter last_build_date : Time?
    getter categories : Array(Category) = Array(Category).new
    getter generator : String?
    getter docs : String?
    getter cloud : Cloud?
    getter ttl : String?
    getter image : Image?
    getter rating : String?
    getter text_input : TextInput?
    getter skip_hours : Array(UInt8) = Array(UInt8).new
    getter skip_days : Array(String) = Array(String).new
  
    def initialize(xml_node : XML::Node)

      # Parse rss->channel from parsed XML.
      rss = xml_node.children.select(&.name.matches?(/^rss$/))[0]
      channel = rss.children.select(&.name.matches?(/^channel$/))[0]

      # Parse the rest of the elements from channel.
      channel.children.select(&.element?).each do |child|
        case child.name
          when "title"
            @title = child.inner_text
          when "link"
            @link = child.inner_text
          when "description"
            @description = child.inner_text

          when "item"
            @items.push(Item.new(child))
          when "language"
            @language = child.inner_text
          when "copyright"
            @copyright = child.inner_text
          when "managingEditor"
            @managing_editor = child.inner_text
          when "webMaster"
            @web_master = child.inner_text
          when "pubDate"
            @pub_date = Time.parse_rfc2822(child.inner_text)
          when "lastBuildDate"
            @last_build_date = Time.parse_rfc2822(child.inner_text)
          when "category"
            @categories.push(Category.new(child))
          when "generator"
            @generator = child.inner_text
          when "docs"
            @docs = child.inner_text
          when "ttl"
            @ttl = child.inner_text
          when "image"
            @image = Image.new(child)
          when "rating"
            @rating = child.inner_text
          when "skipHours"
            @skip_hours = parse_skip(child, "hour").map { |h| h.to_u8 }
          when "skipDays"
            @skip_days = parse_skip(child, "day")
        end
      end

    end

    # Function parses skip_hours or skip_days elements into an array of hours
    # or days to skip.
    # https://www.rssboard.org/skip-hours-days
    def parse_skip(xml_node : XML::Node, period : String)
  
      skip = Array(String).new

      # Loop over children and add skip periods to Array.
      begin
        xml_node.children.select(&.name.matches?(/#{period}/i)).each do |child|
          skip.push(child.inner_text)
        end
      rescue IndexError # Return empty array if <{period}> doesn't exist.
        return skip
      end
  
      return skip
  
    end

  end

  # https://www.rssboard.org/rss-specification#ltimagegtSubelementOfLtchannelgt
  class Image
    include JSON::Serializable

    # Required elements.
    getter url : String?
    getter title : String?
    getter link : String?
    
    # Optional elements.
    getter width : UInt16 = 88
    getter height : UInt16 = 31
    getter description : String?
  
    def initialize(image : XML::Node)

      # Assign instance variables.
      image.children.select(&.element?).each do |child|
        case child.name
          when "url"
            @url = child.inner_text
          when "title"
            @title = child.inner_text
          when "link"
            @link = child.inner_text
          when "width"
            @width = child.inner_text.to_u16
          when "height"
            @height = child.inner_text.to_u16
          when "description"
            @description = child.inner_text
        end
      end

    end
  
  end

  # https://www.rssboard.org/rss-specification#ltcloudgtSubelementOfLtchannelgt
  class Cloud
    include JSON::Serializable

    getter domain : String?
    getter port : String?
    getter path : String?
    getter register_procedure : String?
    getter protocol : String?

    def initialize(xml_node : XML::Node)
      @domain = xml_node["domain"]?
      @port = xml_node["port"]?
      @path = xml_node["path"]?
      @register_procedure = xml_node["register_procedure"]?
      @protocol = xml_node["protocol"]?
    end
  end

  # https://www.rssboard.org/rss-specification#lttextinputgtSubelementOfLtchannelgt
  class TextInput
    include JSON::Serializable

    getter title : String?
    getter description : String?
    getter name : String?
    getter link : String?

    def initialize(xml_node : XML::Node)
      xml_node.children.select(&.element?).each do |child|
        case child.name
          when "title"
            @title = child.inner_text
          when "description"
            @description = child.inner_text
          when "name"
            @name = child.inner_text
          when "link"
            @link = child.inner_text
        end
      end
    end
  end

  # https://www.rssboard.org/rss-specification#hrelementsOfLtitemgt
  class Item
    include JSON::Serializable
  
    getter title : String?
    getter link : String?
    getter description : String?
    getter author : String?
    getter categories : Array(Category) = Array(Category).new
    getter comments : String?
    getter enclosure : Enclosure?
    getter guid : GUID?
    getter pub_date : Time?
    getter source : String?
  
    def initialize(xml_feed : XML::Node)
      xml_feed.children.select(&.element?).each do |child|
        case child.name
          when "title"
            @title = child.inner_text
          when "link"
            @link = child.inner_text
          when "description"
            @description = child.inner_text
          when "author"
            @author = child.inner_text
          when "category"
            @categories.push(Category.new(child))
          when "comments"
            @comments = child.inner_text
          when "enclosure"
            @enclosure = Enclosure.new(child)
          when "guid"
            @guid = GUID.new(child)
          when "pubDate"
            @pub_date = Time.parse_rfc2822(child.inner_text)
          when "source"
            @source = child.inner_text
        end
      end
    end
  end

  # https://www.rssboard.org/rss-specification#ltsourcegtSubelementOfLtitemgt
  class Source
    include JSON::Serializable

    getter url : String?
    getter text : String

    def initialize(xml_node : XML::Node)
      @url = xml_node["url"]?
      @text = xml_node.inner_text
    end
  end

  # https://www.rssboard.org/rss-specification#ltenclosuregtSubelementOfLtitemgt
  class Enclosure
    include JSON::Serializable
  
    getter url : String?
    getter length : String?
    getter type : String?
  
    def initialize(xml_node : XML::Node)
      @url = xml_node["url"]?
      @length = xml_node["length"]?
      @type = xml_node["type"]?
    end
  end

  # https://www.rssboard.org/rss-specification#ltcategorygtSubelementOfLtitemgt
  class Category
    include JSON::Serializable
  
    getter domain : String?
    getter text : String
  
    def initialize(xml_node : XML::Node)
      @domain = xml_node["domain"]?
      @text = xml_node.inner_text
    end
  end

  # https://www.rssboard.org/rss-specification#ltguidgtSubelementOfLtitemgt
  class GUID
    include JSON::Serializable
    
    getter is_permalink : Bool = true
    getter text : String?

    def initialize(xml_node : XML::Node)
      if xml_node["is_permalink"]? != nil
        @is_permalink = xml_node["is_permalink"] == true
      end
      @text = xml_node.inner_text
    end
  end

end
